package org.fishyfish.ext


fun String.beginWithLowerCase(): String {
    return when (this.length) {
        0 -> ""
        1 -> this.toLowerCase()
        else -> this[0].toLowerCase() + this.substring(1)
    }
}

fun String.beginWithUpperCase(): String {
    return when (this.length) {
        0 -> ""
        1 -> this.toUpperCase()
        else -> this[0].toUpperCase() + this.substring(1)
    }
}

fun String.toCamelCase(): String {
    return this.split('_').mapIndexed { index, s ->
        when(index){
            0 -> s.toLowerCase()
            else -> s.beginWithUpperCase()
        }
    }.joinToString("")
}

fun String.toSnakeCase(delimiter:String="-"): String {
    var text: String = ""
    var isFirst = true
    this.forEach {
        if (it.isUpperCase()) {
            if (isFirst) isFirst = false
            else text += delimiter
            text += it.toLowerCase()
        } else {
            text += it
        }
    }
    return text
}



fun <T> Iterable<T>.joinComma(separator: CharSequence = ",\n", transform: ((T) -> CharSequence)? = null): String {
    return joinToString(separator = separator, prefix = "", postfix = "", limit = -1, truncated = "...", transform = transform)
}

fun <T> Iterable<T>.join(separator: CharSequence = "\n", transform: ((T) -> CharSequence)? = null): String {
    return joinToString(separator = separator, prefix = "", postfix = "", limit = -1, truncated = "...", transform = transform)
}
