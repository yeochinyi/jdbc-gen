package org.fishyfish.jdbcgen

import java.sql.Connection
import java.sql.DatabaseMetaData
import java.sql.DriverManager

object Main {

    private val URL = "jdbc:sqlserver://localhost:1433;database=freeman8_vsec"
    private val USERNAME = "sa"
    private val PASSWORD = "P@ssword!23"

    @JvmStatic
    fun main(args: Array<String>) {

        var conn: Connection? = null
        var dbmd: DatabaseMetaData? = null

        conn = DriverManager.getConnection(
            URL, USERNAME,
            PASSWORD
        )
        dbmd = conn!!.metaData
        if (dbmd != null) {
            println("Database Version: " + dbmd.databaseProductVersion)
            println("Driver Name: " + dbmd.driverName)
            println("Driver Version: " + dbmd.driverVersion)
            println("URL: " + dbmd.url)
            println("User Name: " + dbmd.userName)
            println(
                if (dbmd.supportsANSI92FullSQL())
                    "ANSI92FullSQL supported."
                else
                    "ANSI92FullSQL not supported."
            )
            println(
                if (dbmd.supportsTransactions())
                    "Transaction supported."
                else
                    "Transaction not supported."
            )

            val tables = tables(dbmd)
            JavalinAllTablesTemplate.printRouter(tables)
            JavalinAllTablesTemplate.printModule(tables)

            DevXAllTablesTemplate.printNavi(tables)
            DevXAllTablesTemplate.printRouting(tables)

            //tables.forEach(::println)
            tables.forEach {
                JavalinPerTableTemplate.printControllers(it)
                JavalinPerTableTemplate.printRepo(it)

                DevXPerTableTemplate.printHtml(it)
                DevXPerTableTemplate.printTs(it)
            }

        } else {
            println("Metadata not supported")
        }
        conn?.close()

    }

    fun tables(md: DatabaseMetaData): MutableList<DbTable> {

        val allTables = mutableListOf<DbTable>()

        val catalog = "freeman8_vsec"
        //val catalog = "dev"
        var schemaPattern = "dbo"
        val tables = md.getTables(catalog, schemaPattern, "%", arrayOf("TABLE"))

        //val excludeTables = listOf("sysdiagrams","Attendance","AttendanceList","AttendanceTimeSheet","School","Accounts")
        val excludeTables = listOf("sysdiagrams")

        val dataTypes = mutableSetOf<String>()

        while (tables.next()) {

            val tableName = tables.getString("TABLE_NAME")

            if (tableName in excludeTables) continue

            val columns = md.getColumns(catalog, schemaPattern, tableName, "%")

            // (1) TABLE_CAT	:freeman8_vsec,     (2) TABLE_SCHEM	:dbo,   (3) TABLE_NAME	:Accounts,      (4) COLUMN_NAME	:AccountID, (5) DATA_TYPE	:4,
            // (6) TYPE_NAME	:int identity,      (7) COLUMN_SIZE	:10,    (8) BUFFER_LENGTH	:4,         (9) DECIMAL_DIGITS	:0,     (10) NUM_PREC_RADIX	:10,
            // (11) NULLABLE	:0,                 (12) REMARKS	:null,  (13) COLUMN_DEF	:null,          (14) SQL_DATA_TYPE	:4,     (15) SQL_DATETIME_SUB	:null,
            // (16) CHAR_OCTET_LENGTH	:null,      (17) ORDINAL_POSITION	:1, (18) IS_NULLABLE	:NO,    (19) SS_IS_SPARSE	:0,
            // (20) SS_IS_COLUMN_SET	:0,         (21) IS_GENERATEDCOLUMN	:0, (22) IS_AUTOINCREMENT	:YES, (23) SS_UDT_CATALOG_NAME	:null, (24) SS_UDT_SCHEMA_NAME	:null,
            // (25) SS_UDT_ASSEMBLY_TYPE_NAME	:null, (26) SS_XML_SCHEMACOLLECTION_CATALOG_NAME	:null,  (27) SS_XML_SCHEMACOLLECTION_SCHEMA_NAME	:null, (28) SS_XML_SCHEMACOLLECTION_NAME	:null, (29) SS_DATA_TYPE	:56,

            //val selectedColumnIndice = 1..columns.metaData.columnCount
            val selectedColumnIndice = arrayOf(4, 5, 6, 7,9, 11, 22)

            val allColumns = mutableListOf<DbTableColumn>()

            while (columns.next()) {

                dataTypes.add(columns.getString(6))

                for (i in selectedColumnIndice) {
                    with(columns.metaData) {
                        val columnLabel = getColumnLabel(i)
                        //print("($i) ${columnLabel}\t:${columns.getString(i)}, ")
                    }
                }
                //  println()

                val name = columns.getString(4)
                val typeName = columns.getString(6)

                val type = if (typeName.contains(' ')) typeName.split(" ") else listOf(typeName, "")
                val size = columns.getInt(7)
                val nullable = columns.getInt(11)
                val autoIncrement = columns.getString(22)
                val scale = columns.getInt(9)

                allColumns += DbTableColumn(
                    name,
                    ColumnDataType.getType(type[0])!!,
                    type[1].isNotBlank(),
                    size,
                    nullable == 1,
                    autoIncrement == "YES",
                    scale
                )
            }

            allTables += DbTable(tableName, allColumns)

            //println("dataTypes=$dataTypes")
        }

        return allTables


    }


    fun toJdl(table: DbTable): String {
        with(table) {
            return """
           entity ${name} {
            ${columns.joinToString(separator = ",\n") { toJdl(it) }}
            }
        """.trimIndent()
        }
    }

    fun toJdl(column: DbTableColumn): String {
        return """
           ${column.name} ${column.type.jdlString}
        """.trimIndent()
    }


    enum class ColumnDataType(val jdbcString: List<String>, val jdlString: String, val exposed: String, val devX: String) {
        VARCHAR(listOf("varchar", "nvarchar"), "String", "varchar", "string"),
        BIT(listOf("bit"), "Boolean", "bool", "boolean"),

        INT(listOf("int"), "Int", "integer", "number"),
        BIGINT(listOf("bigint"), "Long", "long", "number"),
        FLOAT(listOf("float"), "Double", "double", "number"),
        MONEY(listOf("money"), "BigDecimal", "decimal", "number"),

        DATETIME(listOf("datetime"), "DateTime", "datetime", "datetime"),
        DATE(listOf("date"), "DateTime", "date", "date")
        ;

        //fun hasLength() = this == VARCHAR

        companion object {
            fun getType(jdbcString: String): ColumnDataType? {
                return values().firstOrNull { jdbcString in it.jdbcString }
            }
        }

    }

    data class DbTableColumn(
        val name: String,
        val type: ColumnDataType,
        val pk: Boolean,
        val size: Int,
        val nullable: Boolean,
        val autoIncrement: Boolean,
        val scale: Int
    )

    data class DbTable(val name: String, val columns: List<DbTableColumn>) {

        fun getPk() =
            columns.firstOrNull { it.pk }

        override fun toString(): String {
            return """
        Name.Table($name)
                ${columns.joinToString(separator = "\n") { it.toString() }}
            """
        }

    }

}