package org.fishyfish.jdbcgen

import org.fishyfish.ext.beginWithLowerCase
import org.fishyfish.ext.join
import org.fishyfish.ext.joinComma
import org.fishyfish.ext.toSnakeCase
import java.io.File

object DevXAllTablesTemplate {

    fun printNavi(tables: MutableList<Main.DbTable>) {

        val file = File("D:\\temp\\vivakids\\gen\\devx\\app-navigation.kt")
        file.parentFile.mkdirs()
        file.createNewFile()

            file.writeText("""
export const navigation = [
  {
    text: 'Home',
    path: '/home',
    icon: 'home'
  },
  {
    text: 'Data',
    icon: 'folder',
    items: [
      {
        text: 'Profile',
        path: '/profile'
      },
${tables.join { """
      {
        text: '${it.name}',
        path: '/${it.name.toSnakeCase("-")}'
      },
"""}}      

    ]
  }
];

"""
        )
    }
    fun printRouting(tables: MutableList<Main.DbTable>) {

        val file = File("D:\\temp\\vivakids\\gen\\devx\\app-routing.module.ts")
        file.parentFile.mkdirs()
        file.createNewFile()

        file.writeText("""
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginFormComponent } from './shared/components';
import { AuthGuardService } from './shared/services';
import { HomeComponent } from './pages/home/home.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { DxDataGridModule, DxFormModule } from 'devextreme-angular';

${tables.join { """import { ${it.name}Component } from './pages/${it.name.toSnakeCase()}/${it.name.toSnakeCase()}.component';"""}}

import { AgGridModule } from 'ag-grid-angular';
import { CommonModule } from '@angular/common';

const routes: Routes = [

${tables.joinComma { 
    """{
    path: '${it.name.toSnakeCase()}',
    component: ${it.name}Component,
    canActivate: [ AuthGuardService ]
  }"""}}
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'login-form',
    component: LoginFormComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: '**',
    redirectTo: 'home',
    canActivate: [ AuthGuardService ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    DxDataGridModule, DxFormModule,
    AgGridModule.withComponents([]),
    CommonModule
  ],
  providers: [AuthGuardService],
  exports: [RouterModule],
  declarations: [
    HomeComponent, 
    ProfileComponent, 
    SchoolComponent, 
${tables.joinComma { """${it.name}Component"""}}
  ]
})
export class AppRoutingModule { }
"""
        )
    }


}