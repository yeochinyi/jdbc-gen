package org.fishyfish.jdbcgen

import org.fishyfish.ext.beginWithLowerCase
import org.fishyfish.ext.join
import org.fishyfish.ext.joinComma
import java.io.File

object JavalinPerTableTemplate {

    fun printControllers(table: Main.DbTable) {

        val file = File("D:\\temp\\vivakids\\gen\\kt\\controllers\\${table.name}Controller.kt")
        file.parentFile.mkdirs()
        file.createNewFile()

        with(table) {
            file.writeText(
                """
package com.viva.app.web.controllers

import com.viva.app.domain.repository.${table.name}
import com.viva.app.domain.repository.${table.name}Repository
import io.javalin.Context


class ${table.name}Controller(
        repository: ${table.name}Repository
) : AbstractController<${table.name}>(repository) {

    fun upsert(ctx: Context) {
        upsert(ctx, ${table.name}::class.java)
    }
}
"""//.trimIndent()
            )
        }
    }

    fun printRepo(table: Main.DbTable) {

        val file = File("D:\\temp\\vivakids\\gen\\kt\\repository\\${table.name}Repository.kt")
        file.parentFile.mkdirs()
        file.createNewFile()

        with(table) {
            file.writeText(
                """
package com.viva.app.domain.repository

import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.statements.UpdateBuilder
import org.joda.time.DateTime
import java.math.BigDecimal
import javax.sql.DataSource
    
data class ${name}(
    ${columns.joinComma {
            """var ${it.name.beginWithLowerCase()}: ${it.type.jdlString}? = null"""
    }}
): AbstractDbData {
    override var id: Number?
        get() = this.${getPk()?.name?.beginWithLowerCase()}
        set(i) {
            this.${getPk()?.name?.beginWithLowerCase()} = i as ${getPk()?.type?.jdlString}
        }
} 

class ${name}Repository(dataSource: DataSource) : AbstractTable<${name}>("${name}", dataSource){
    ${columns.join { 
                    with(it) {
"""
    val ${name.beginWithLowerCase()}: Column<${type.jdlString}> = ${type.exposed}("$name"
        ${if (type == Main.ColumnDataType.VARCHAR) ", $size" else ""}
        ${if (type == Main.ColumnDataType.MONEY) ", $size, $scale" else ""}
    )
    ${if (pk) ".autoIncrement().primaryKey()" else ""}
"""
                            .replace("\n", "")
                    }
                }
                }

    override fun toDomain(row: ResultRow): ${name} {
        return ${name}(
            ${columns.joinToString(separator = ",\n\t") { col ->
                    with(col) {
                        val fieldname = name.beginWithLowerCase()
                        """
                        ${fieldname} = row[${fieldname}]
                    """.trimIndent()
                    }
                }}
        )
    }

    override fun fromDomain(it: UpdateBuilder<*>, data: ${name}) {
        ${columns.filter { !it.pk }
                    .joinToString(separator = "\n\t") { col ->
                        with(col) {
                            val fieldname = name.beginWithLowerCase()
                            """
                    it[${fieldname}] = data.${fieldname}!!
                """.trimIndent()
                        }
                    }
                }
    }

    init {
        initColumnMap<${name}Repository>()
    }
}
"""//.trimIndent()
            )
        }
    }
}