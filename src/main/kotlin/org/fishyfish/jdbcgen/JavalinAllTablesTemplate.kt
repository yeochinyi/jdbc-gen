package org.fishyfish.jdbcgen

import org.fishyfish.ext.beginWithLowerCase
import org.fishyfish.ext.join
import org.fishyfish.ext.joinComma
import org.fishyfish.ext.toSnakeCase
import java.io.File

object JavalinAllTablesTemplate {

    fun printRouter(tables: MutableList<Main.DbTable>) {

        val file = File("D:\\temp\\vivakids\\gen\\kt\\Router.kt")
        file.parentFile.mkdirs()
        file.createNewFile()

        file.writeText(
            """
package com.viva.app.web

import com.viva.app.config.Roles
${tables.join { """import com.viva.app.web.controllers.${it.name}Controller""" }}
import com.viva.app.web.controllers.UserController
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.*
import io.javalin.security.SecurityUtil.roles
import org.koin.standalone.KoinComponent

class Router(
        private val userController: UserController,
${tables.joinComma { """private val ${it.name.beginWithLowerCase()}Controller: ${it.name}Controller""" }}
) : KoinComponent {

    fun register(app: Javalin) {
        val rolesOptionalAuthenticated = roles(Roles.ANYONE, Roles.AUTHENTICATED)
        val readRoles = roles(Roles.AUTHENTICATED)
        val writeRoles = roles(Roles.AUTHENTICATED)
        app.routes {
            path("users") {
                //post(userController::register, roles(Roles.ANYONE))
                post("login", userController::login, roles(Roles.ANYONE))
                //get(userController::findAll, roles(Roles.ANYONE))
            } 
${tables.join {
                """
            path("${it.name.toSnakeCase()}s") {
                get(${it.name.beginWithLowerCase()}Controller::findAll, readRoles)
                post(${it.name.beginWithLowerCase()}Controller::upsert, writeRoles)
            }
            path("${it.name.toSnakeCase()}s/:id") {
                put(${it.name.beginWithLowerCase()}Controller::upsert, writeRoles)
            }
"""
            }
            }
        }
    }
}
"""
        )
    }

    fun printModule(tables: MutableList<Main.DbTable>) {

        val file = File("D:\\temp\\vivakids\\gen\\kt\\ModulesConfig.kt")
        file.parentFile.mkdirs()
        file.createNewFile()

        file.writeText(
            """
package com.viva.app.config

import com.viva.app.config.Roles
${tables.join {
                """import com.viva.app.web.controllers.${it.name}Controller
import com.viva.app.domain.repository.${it.name}Repository"""
            }}

import com.viva.app.domain.repository.UserRepository
import com.viva.app.domain.service.UserService
import com.viva.app.utils.JwtProvider
import com.viva.app.web.Router

import org.koin.dsl.module.module

object ModulesConfig {
    private val configModule = module {
        single { AppConfig() }
        single { JwtProvider() }
        single { AuthConfig(get()) }
        single {
            DbConfig(
                    getProperty("jdbc.url"),
                    getProperty("db.username"),
                    getProperty("db.password")
            ).getDataSource()
        }
        single { Router(
            get(), get(), ${tables.join(",") { "get()" }}) 
        }
    }
    private val userModule = module {
        single { UserController(get()) }
        single { UserService(get(), get()) }
        single { UserRepository(get()) }
    }

${tables.join {
                """
    private val ${it.name.beginWithLowerCase()}Module = module {
        single { ${it.name}Controller(get()) }
        single { ${it.name}Repository(get()) }
    }
"""
            }}

    internal val allModules = listOf(
            configModule,
            userModule, 
${tables.joinComma { """${it.name.beginWithLowerCase()}Module""" }}
    )
}"""
        )
    }


}