package org.fishyfish.jdbcgen

import org.fishyfish.ext.beginWithLowerCase
import org.fishyfish.ext.toSnakeCase
import java.io.File

object DevXPerTableTemplate {

    fun printTs(table: Main.DbTable) {

        val file = File("D:\\temp\\vivakids\\gen\\devx\\${table.name.toSnakeCase()}\\${table.name.toSnakeCase()}.component.ts")
        file.parentFile.mkdirs()
        file.createNewFile()

        with(table) {
            file.writeText(
                """
import { Component, ChangeDetectionStrategy } from '@angular/core';
import { AbstractRestComponent } from 'src/app/shared/components/abstract-rest-component';
import {Injector} from '@angular/core';

@Component({
    templateUrl: '${table.name.toSnakeCase()}.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ${table.name}Component extends AbstractRestComponent {
    constructor(injector: Injector) {
        super(injector, '${table.name.toSnakeCase()}s', '${table.getPk()?.name?.beginWithLowerCase()}')
    }
}
"""//.trimIndent()
            )
        }
    }

    fun printHtml(table: Main.DbTable) {

        val file = File("D:\\temp\\vivakids\\gen\\devx\\${table.name.toSnakeCase()}\\${table.name.toSnakeCase()}.component.html")
        file.parentFile.mkdirs()
        file.createNewFile()

        with(table) {
            file.writeText(
                """
<dx-data-grid class="dx-card wide-card"
    
    [dataSource]="dataSource"  
    [remoteOperations]="true"   

    [showColumnLines]="false"
    [showRowLines]="true"
    [showBorders]="true"
    [rowAlternationEnabled]="true"

    [allowColumnReordering]="true"
    [allowColumnResizing]="true"
    [columnAutoWidth]="true"
    >

        <!-- [focusedRowEnabled]="true"
    [focusedRowIndex]="0"
    [columnAutoWidth]="true"
    [columnHidingEnabled]="true"     -->

    <dxo-column-chooser [enabled]="true"></dxo-column-chooser>
    <dxo-column-fixing [enabled]="true"></dxo-column-fixing>
    <dxo-state-storing [enabled]="true" type="localStorage" storageKey="storage"></dxo-state-storing>

    <dxo-export [enabled]="true" fileName="dataExport" [allowExportSelectedData]="true"></dxo-export>

    <dxo-sorting mode="single"></dxo-sorting>
    <dxo-search-panel 
        [visible]="true" 
        [width]="240" 
        placeholder="Search..."></dxo-search-panel>    
    
    <dxo-paging [pageSize]="5"></dxo-paging>
    <dxo-pager 
        [visible]="true"
        [showPageSizeSelector]="true"
        [allowedPageSizes]="[5, 10, 20]"
        [showInfo]="true">
    </dxo-pager>

    <dxo-filter-row [visible]="true"></dxo-filter-row>

    <dxo-editing
        mode="form"
        [allowAdding]="true"
        [allowUpdating]="true"
        [allowDeleting]="false">
    </dxo-editing>

        ${columns.joinToString(separator = "\n") { col ->
                    with(col) {
                        val fieldname = name.beginWithLowerCase()
                        """
<dxi-column dataField="${fieldname}" dataType="${col.type.devX}"></dxi-column>
""".trimIndent()
                    }
                }
                }

"""//.trimIndent()
            )
        }
    }
}