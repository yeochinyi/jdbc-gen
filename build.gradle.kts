import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.21"
}

group = "org.fishyfish"
version = "1.0.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("com.microsoft.sqlserver", "mssql-jdbc", "7.2.2.jre8")



}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}